Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get "offers/search", to: "offers#search", as: "offers_search"
    end
  end
end
