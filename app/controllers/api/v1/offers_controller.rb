class Api::V1::OffersController < ApplicationController
    # GET /results
    
    def search
        acceptable_categories = [1,2,4].freeze
        offers = Offers.nearby()
        new_search = create_new_search
        if new_search.search
            # Get the nearest 2 offers across all remaining offers
            render json: {
                "offers": new_search.results
            }
        else
            render json: {
                "errors": new_search.errors
            }
        end
    end

    private
    def create_new_search
        OfferSearchService.new({
            check_in_date: params["check_in_date"]
        })
    end
end