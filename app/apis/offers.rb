class Offers
    include HTTParty
    
    base_uri 'https://5dbf8a76e295da001400b642.mockapi.io/offers'

    def self.nearby(lat="1.313492", lon="103.860359", rad="20", options = {})
        url = "near_by?lat=#{lat}&lon=#{lon}&rad=#{rad}"
        self.get(url, options)['offers']
    end
end