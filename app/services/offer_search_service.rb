class OfferSearchService
    attr_reader :check_in_date, :results, :errors
    
    # Validations
    include ActiveModel::Validations
    validate :check_date_nil_or_empty
    validate :check_date_valid_format

    def initialize(params)
        @check_in_date = params[:check_in_date]
        @errors = []
        @results = []
    end
    
    def search
        return false if not self.valid?
        acceptable_categories = [1,2,4].freeze
        check_in_date = Date.parse(@check_in_date)
        offers = Offers.nearby()
        # remove unrelated categories first
        offers.reject! { |offer| not acceptable_categories.include?(offer["category"]) }
        # select all offers within date range
        offers.reject! { |offer| Date.parse(offer["valid_to"]) < (check_in_date + 5.days) }
        # Take only the nearest merchant for each offer
        offers.each do |offer|
            merchants = offer["merchants"]
            offer["merchants"] = merchants.min_by {|k| k["distance"] } if merchants.length > 1
        end
        # Create augmented group_by
        category_offers = {}
        offers.each do |offer|
            category = offer["category"]
            category_offers[category] = [] if category_offers[category].nil?
            category_offers[category].append offer
        end
        # Sort augmented group_by by offers closest in each category
        category_offers.keys.each do |key|
            o_list = category_offers[key]
            nearest_in_category = o_list.min_by { |k| k["distance"] }
            @results.append(nearest_in_category)
        end
        @results = @results.sort_by { |v| v["distance"]}.take(2)
        return true
    end

    private
    def check_date_nil_or_empty
        if @check_in_date.nil?
            self.errors << "Check in date is missing"
            throw(:abort)
        elsif @check_in_date.empty?
            self.errors << "Check in date can't be empty"
            throw(:abort)
        end
    end

    def check_date_valid_format
        begin
            Date.strptime(@check_in_date, "%Y-%m-%d")
        rescue TypeError, ArgumentError => te
            self.errors << "must be a valid date with format YYYY-MM-DD"
            throw(:abort)
        end
    end

    # private
    # def validate_params
    #     if @check_in_date.nil?
    #         @errors.append("Missing check in date")
    #         return false
    #     elsif @check_in_date.empty?
    #         @errors.append("Empty check in date")
    #         return false
    #     end
    #     begin
            
    #     rescue => exception
            
    #     end
    #     return true
    # end
end