require 'rails_helper'

describe "get all offers route", :type => :request do
    before { get '/api/v1/offers/search?check_in_date=2019-12-25' }

    it 'returns all offers on check-in date 2019-12-25' do
        expect(JSON.parse(response.body)["offers"].size).to eq(2)
    end
    it 'returns status code 200' do
        expect(response).to have_http_status(:success)
    end
end