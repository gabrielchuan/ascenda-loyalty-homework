# README

## Info

This is my submission for Ascenda Loyalty's homework.

No databases are involved as this is an Integration API of sorts. We take data from a specified endpoint by creating an API wrapper. Then we process that information given a specific business logic and return the results to the caller.

## Setup

1. Run `bundle install`
2. Run `rails s`
3. Invoke a GET request at `http://localhost:3000/api/v1/offers/search` or use `http://localhost:3000/api/v1/offers/search?check_in_date=2019-12-25`


## Tests

Run `rspec`